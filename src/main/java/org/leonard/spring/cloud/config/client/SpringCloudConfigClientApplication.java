package org.leonard.spring.cloud.config.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Slf4j
public class SpringCloudConfigClientApplication implements CommandLineRunner {

	@Value("${hello.user}")
	private String user;

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfigClientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("-------------- application start -------------");
		System.out.println("Say hello: " + user);
		System.out.println("-----------------------------------------------");
	}
}
